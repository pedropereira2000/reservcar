package code_coverage;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.Test;

import Dao.LoginDAO;

class loginDao_test {

	@Test
	void test_01() {
		var lgDao = new LoginDAO();
		var res = assertThrows(Exception.class, () -> {
			lgDao.checkLogin(" ", "123");
		}).getMessage();
		assertEquals("ERRO! Login e senha nao preenchidos", res);
	}
	
	@Test
	void test_02() throws SQLException {
		var lgDao = new LoginDAO();
		lgDao.checkLogin("adm", "123");
	}
	
	@Test
	void test_03() {
		var lgDao = new LoginDAO();
		var res = assertThrows(Exception.class, () -> {
			lgDao.consultarLogin(" ");
		}).getMessage();
		assertEquals("ERRO! Login inserido esta invalido", res);
	}
	
	@Test
	void test_04() throws SQLException {
		var lgDao = new LoginDAO();
		lgDao.consultarLogin("adm");
	}

}
