
package View;

import java.sql.*;
import Connection.ConnectionFactory;
import javax.swing.JOptionPane;


public class TelaPrincipal extends javax.swing.JFrame {

    Connection conexao = null;
    
    public TelaPrincipal() {
        initComponents();
        //Verificação qual tipo de permissão possui o servidor
        if(TelaLogin.permissao.equals("Funcionario")){
            //Travando botão de gerenciar funcionários se a permissão for funcionário
            btnFuncionario.setEnabled(false);
        }  
        conexao = ConnectionFactory.conectar();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblImage = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        btnFuncionario = new javax.swing.JButton();
        txtNome = new javax.swing.JLabel();
        btnReturn = new javax.swing.JButton();
        btnFinalizar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(255, 255, 255));
        setName("FramePrincipal"); // NOI18N
        setUndecorated(true);

        lblImage.setToolTipText("");

        jPanel1.setEnabled(false);
        jPanel1.setLayout(null);

        btnFuncionario.setText("MANTER FUNCIONARIOS");
        btnFuncionario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFuncionarioActionPerformed(evt);
            }
        });
        jPanel1.add(btnFuncionario);
        btnFuncionario.setBounds(50, 130, 340, 40);

        txtNome.setFont(new java.awt.Font("Roboto", 1, 14)); // NOI18N
        txtNome.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txtNome.setText(TelaLogin.userNome);
        txtNome.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                txtNomeComponentShown(evt);
            }
        });
        jPanel1.add(txtNome);
        txtNome.setBounds(310, 60, 79, 17);

        btnReturn.setText("TROCAR USUARIO");
        btnReturn.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnReturn.setMaximumSize(new java.awt.Dimension(105, 23));
        btnReturn.setMinimumSize(new java.awt.Dimension(105, 23));
        btnReturn.setName("btnReturn"); // NOI18N
        btnReturn.setPreferredSize(new java.awt.Dimension(105, 23));
        btnReturn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnReturnActionPerformed(evt);
            }
        });
        jPanel1.add(btnReturn);
        btnReturn.setBounds(50, 200, 340, 40);
        btnReturn.getAccessibleContext().setAccessibleName("");
        btnReturn.getAccessibleContext().setAccessibleDescription("");

        btnFinalizar.setText("FINALIZAR APLICACAO");
        btnFinalizar.setMaximumSize(new java.awt.Dimension(105, 23));
        btnFinalizar.setMinimumSize(new java.awt.Dimension(105, 23));
        btnFinalizar.setPreferredSize(new java.awt.Dimension(105, 23));
        btnFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinalizarActionPerformed(evt);
            }
        });
        jPanel1.add(btnFinalizar);
        btnFinalizar.setBounds(50, 270, 340, 40);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48))
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 448, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lblImage, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52))
        );

        setSize(new java.awt.Dimension(445, 377));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    //Evento do botão para sair e trocar o servidor
    private void btnReturnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnReturnActionPerformed
        // TODO add your handling code here:
        new TelaLogin().show();
        dispose();
    }//GEN-LAST:event_btnReturnActionPerformed

    //Evento do botão funcionário para gerenciar os funcionários
    private void btnFuncionarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFuncionarioActionPerformed
        // TODO add your handling code here:
        new TelaServidorView().show();
        dispose();
    }//GEN-LAST:event_btnFuncionarioActionPerformed

    private void txtNomeComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_txtNomeComponentShown
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txtNomeComponentShown

    private void btnFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinalizarActionPerformed
        int confire = JOptionPane.showConfirmDialog(null, "Deseja realmente finalizar a aplicacao?","Atencao",JOptionPane.YES_NO_OPTION);
        if(confire == JOptionPane.YES_OPTION){
            JOptionPane.showMessageDialog(null, "Aplicacao encerrada");
            System.exit(0);
        }
    }//GEN-LAST:event_btnFinalizarActionPerformed

    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TelaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFinalizar;
    private javax.swing.JButton btnFuncionario;
    private javax.swing.JButton btnReturn;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblImage;
    private javax.swing.JLabel txtNome;
    // End of variables declaration//GEN-END:variables
}
