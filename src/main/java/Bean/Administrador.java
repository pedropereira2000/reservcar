package Bean;

public class Administrador extends Servidor{
    private Ambiente ambi;
    
    public Administrador() {
        nome = "";
        siape = 0;
        email = "";
        ramal = 0;
        senha = "";
        permissao = "";
        conPessoal = "";
        login = "";
    }

    public Administrador(String nome, int siape, String email, int ramal, String senha, String permissao, String conPessoal, String login) {
        this.nome = nome;
        this.siape = siape;
        this.email = email;
        this.ramal = ramal;
        this.senha = senha;
        this.permissao = permissao;
        this.conPessoal = conPessoal;
        this.login = login;
    }

    public Ambiente getAmbi(){
        return ambi;
    }

    public void setAmbi(Ambiente ambi){
        this.ambi = ambi;
    }

    public void cadastrarPermissao(){

    }
}
